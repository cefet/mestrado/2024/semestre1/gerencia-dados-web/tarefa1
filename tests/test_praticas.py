"""
Testes para prática 1.
a) Selecionar as instituições dos autores do artigo;
b) Selecionar todos os parágrafos das seções do artigo;
c) Selecionar nomes dos autores do artigo propriamente dito
    e das referências bibliográficas;
d) Selecionar pai do elemento endereço;
e) Selecionar avô do elemento parágrafo;
f) Selecionar todas as ocorrências de endereço.

Testes para prática 2.
a) Selecionar o autor cujo nome é Maria Ana;
b) Selecionar a obra da bibliografia cujo ano é 1999 e o local é
    University of Pennsylvania;
c) Selecionar a seção cujo número é s2 e que contém um parágrafo
    cujo conteúdo é ... ;
d) Selecionar o atributo título das seções.

Testes para prática 3.
a) Retorne todas as seções do artigo que possuem pelo menos um
    sub-elemento figura;
b) Selecione as seções ímpares;
c) Selecione as seções ímpares que possuem pelo menos um sub-elemento figura;
d) Encontre autores que possuam "Ana" no nome.
    Não devem estar nas referências bibliográficas;
e) A versão do artigo;
f) Selecione o parágrafo que tenha ambos os atributos "numero" e "tipo";
g) Um parágrafo cujo tamanho da string que ele contém é 27;
h) Selecione o terceiro parágrafo de uma seção;
i) Selecione todo e qualquer parágrafo que tenha a string "two years".
    Trate o uso de maiúsculas e minúsculas!!;
j) Selecionar seções que contenham somente dois parágrafos;
k) Selecione os elementos "ano" descendentes de bibliografia.
"""

import pytest
from tarefa1.xpath import parse


def artigo():
    file_text = 'artigo.xml'
    file_xpath = 'xpath.txt'
    file_golden = 'ouro.xml'

    queries, results, golden = parse(file_text, file_xpath, file_golden)

    for query, result, element in zip(queries, results, golden):
        yield query, result, element


@pytest.mark.parametrize(
    'query, result, golden', list(artigo()), ids=[
        'test_pratica1_instituicoes_autores',
        'test_pratica1_todos_paragrafos_secoes',
        'test_pratica1_nomes_autores_artigo_referencias_bibliograficas',
        'test_pratica1_pai_elemento_endereco',
        'test_pratica1_avo_elemento_paragrafo',
        'test_pratica1_todas_ocorrencias_endereco',
        'test_pratica2_autor_cujo_nome_maria_ana',
        'test_pratica2_obra_bibliografia_cujo_ano_1999_local_university_Pennsylvania',
        'test_pratica2_secao_cujo_numero_s2_contem_paragrafo_cujo_conteudo_elipse',
        'test_pratica2_atributo_titulo_secoes',
        'test_pratica3_todas_secoes_possuem_pelo_menos_um_subelemento_figura',
        'test_pratica3_secoes_impares',
        'test_pratica3_secoes_impares_possuem_pelo_menos_um_subelemento_figura',
        'test_pratica3_autores_possuam_ana_nome_fora_referencias_bibliograficas',
        'test_pratica3_versao',
        'test_pratica3_paragrafo_com_ambos_atributos_numero_tipo',
        'test_pratica3_paragrafo_cujo_tamanho_27',
        'test_pratica3_terceiro_paragrafo_secao',
        'test_pratica3_paragrafos_contenham_two_years_maiusculas_minusculas',
        'test_pratica3_secoes_contenham_somente_dois_paragrafos',
        'test_pratica3_elementos_ano_descendentes_bibliografia',
    ]
)
def test_xpath_execution(query, result, golden):
    message = f'XPath query: {query.strip()}'

    assert result == golden, message
