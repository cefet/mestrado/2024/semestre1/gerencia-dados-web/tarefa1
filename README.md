# Tarefa 1 - Construção de expressões XPath
Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)

Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Gerência de Dados da Web

Professores: Thiago Magela Rodrigues Dias e Gray Farias Moita

Estudante: André Almeida Rocha


## Instalar o projeto
### Criar ambiente virtual:
```sh
python3 -m venv .venv
```
Referência: [Criando ambientes virtuais](https://docs.python.org/pt-br/dev/library/venv.html#creating-virtual-environments)


### Ativar o ambiente virtual:
```sh
source .venv/bin/activate
```
Referência: [Como funcionam os venvs](https://docs.python.org/pt-br/dev/library/venv.html#how-venvs-work)

### Instalar projeto:
```sh
pip3 install .
```

## Visualizar os resultados das expressões XPath
```sh
python3 tarefa1/xpath.py artigo.xml xpath.txt ouro.xml
```
> artigo.xml: arquivo a ser consultado;
>
> xpath.txt: arquivo com as consultar XPath;
>
> ouro.xml: arquivo com os resultados de referência.

## Testar os resultados das expressões XPath com o resultado ouro
### Instalar bibliotecas de teste:
```sh
pip3 install pytest pytest-clarity
```

### Executa os testes exibindo os detalhes:
```sh
pytest tests/test_praticas.py -vv
```

> Você pode desativar um ambiente virtual digitando `deactivate` em seu shell.