"""
Federal Center for Technological Education of Minas Gerais (CEFET-MG)
The Graduate Program in Mathematical and Computational Modeling (PPGMMC)

Web Data Management (GDW)
Professors: Thiago Magela Rodrigues Dias, Gray Farias Moita
Student: André Almeida Rocha

Construction of XPath queries.
"""

from parsel import Selector
from rich import print
import sys
from typing import Iterator, TextIO


def get_text(file_text: str):
    """
    Gets text from file.

    Args:
        file_text (str): Filename of reference text.
    """
    with open(file_text, 'r') as file:
        for line in file:
            yield line.strip()


def get_selector(file_text: str):
    """
    Creates a XML selector from text file.

    Args:
        file_text (str): Filename of reference text.
    """
    separator = str()
    text = separator.join(get_text(file_text))

    selector = Selector(text=text)

    return selector


def get_queries(file_xpath: str) -> list[str]:
    """
    Gets the XPath queries from file.

    Args:
        file_xpath (str): Filename of XPath queries.

    Yields:
        list[str]: XPath queries.
    """
    with open(file_xpath, 'r') as file:
        queries = file.readlines()

    return queries


def run(selector: Selector, queries: list[str]) -> Iterator[list[str]]:
    """
    Executes the XPath queries.

    Args:
        selector (Selector): Selector for parsing.
        queries (list[str]): List of XPath queries.

    Yields:
        Iterator[list[str]]: Results for each XPath query.
    """
    for query in queries:
        yield selector.xpath(query).getall()


def get_elements(file: TextIO) -> Iterator[str]:
    """
    Gets elements separated by double newline from file.

    Args:
        file (TextIO): Open text file to get the elements.

    Yields:
        Iterator[str]: Element striped.
    """
    for element in file:
        if element == '\n':
            break

        yield element.strip()


def get_golden(file_golden: str, total: int) -> Iterator[list[str]]:
    """
    Gets the golden results for each XPath queries.

    Args:
        file_golden (str): Filename of golden XPath queries results.
        total (int): Total of XPath queries results.
    """
    with open(file_golden, 'r') as file:
        for result_index in range(total):
            yield list(get_elements(file))


def show(parsed):
    """
    Shows the result elements for each XPath query.
    """
    for query, result, _ in parsed:
        elements = '\n'.join(result)
        print(f'{query.strip()}\n{elements}\n')


def parse(
    file_text: str, file_xpath: str, file_golden: str
) -> tuple[list[str], list[str], list[str]]:
    """
    Parses the XPath queries and gets golden results.

    Args:
        file_text (str): Filename of XML reference text.
        file_xpath (str): Filename of XPath queries.
        file_golden (str): Filename of golden XPath queries results.
    """
    selector = get_selector(file_text)
    queries = get_queries(file_xpath)
    results = run(selector, queries)
    golden = get_golden(file_golden, total=len(queries))

    return queries, results, golden


def main(file_text: str, file_xpath: str, file_golden: str):
    """
    Execute the XPath queries and show results.

    Args:
        file_text (str): Filename of XML reference text.
        file_xpath (str): Filename of XPath queries.
        file_golden (str): Filename of golden XPath queries results.
    """
    parsed = parse(file_text, file_xpath, file_golden)
    show(parsed)


if __name__ == '__main__':
    if len(sys.argv) != 4:
        usage = f'USAGE:\npython {sys.argv[0]} file_text file_xpath'
        sys.exit(usage)

    _, file_text, file_xpath, file_golden = sys.argv
    main(file_text, file_xpath, file_golden)
